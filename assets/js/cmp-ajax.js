document.addEventListener("DOMContentLoaded", () => {
  const simulateDeleteButton = document.querySelector("#simulate-delete");
  const realDeleteButton = document.querySelector("#real-delete");
  const resultsContainer = document.querySelector("#cmp-simulation-results");

  if (simulateDeleteButton) {
    simulateDeleteButton.addEventListener("click", (event) => {
      event.preventDefault();
      sendDeleteRequest(true); // true pour simuler
    });
  }

  if (realDeleteButton) {
    realDeleteButton.addEventListener("click", (event) => {
      event.preventDefault();
      if (
        window.confirm(
          "Êtes-vous sûr de vouloir supprimer ces images ?\nCette action est irréversible."
        )
      ) {
        sendDeleteRequest(false); // false pour delete
      }
    });
  }
});

const sendDeleteRequest = (simulate) => {
  const formData = new URLSearchParams({
    action: simulate ? "simulate_delete_images" : "delete_media",
    nonce: cmpAjax.nonce,
    simulate: simulate,
  });

  fetch(cmpAjax.ajaxUrl, {
    method: "POST",
    credentials: "same-origin",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: formData,
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Réponse réseau non OK");
      }
      return response.json();
    })
    .then((data) => {
      const resultsContainer = document.querySelector(
        "#cmp-simulation-results"
      );
      if (data.success) {
        if (simulate && data.data.total === 0) {
          message = "Aucun média image à supprimer.";
        } else if (simulate && data.data && data.data.ids) {
          const ids = data.data.ids.join(", ");
          resultsContainer.innerHTML = `<p>Les images avec l'<strong>ID</strong> suivantes seront supprimées <strong>(Total : ${data.data.total})</strong> :<br> ${ids}.</p>`;
        } else if (!simulate) {
          const message =
            data.data && data.data.message
              ? data.data.message
              : "Médias images supprimés avec succès.";
          resultsContainer.innerHTML = `<p>${message}</p>`;
        }
        resultsContainer.style.display = "block";
      } else {
        const errorMessage =
          data.data && data.data.message
            ? data.data.message
            : "Une erreur est survenue.";
        resultsContainer.innerHTML = `<p>Erreur : ${errorMessage}</p>`;
        resultsContainer.style.display = "block";
      }
    })
    .catch((error) => {
      const resultsContainer = document.querySelector(
        "#cmp-simulation-results"
      );
      resultsContainer.innerHTML = `<p>Erreur AJAX: ${error.message}</p>`;
      resultsContainer.style.display = "block";
    });
};
