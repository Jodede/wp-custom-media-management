<?php

function cmp_add_admin_menu()
{
    add_menu_page('Gestion des Médias non offloaded', 'Gestion Médias', 'manage_options', 'wp_custom_media_management', 'cmp_options_page');
}

add_action('admin_menu', 'cmp_add_admin_menu');

function cmp_options_page()
{
    echo '<section id="cmp-container"><h2>Gestion des Médias non offloaded</h2>';

    $filesExist = process_post_requests();

    $buttonText = $filesExist ? 'Régénérer les fichiers' : 'Générer les fichiers';
    echo '<form method="post"><button type="submit" name="generate_files" class="button button-primary">' . $buttonText . '</button></form><br>';

    display_download_options();

    if ($filesExist) {
        display_deletion_options();
    }

    echo '</section>';
}

function process_post_requests()
{
    $types = ['all' => 'Tout', 'images' => 'Images', 'others' => 'Autres'];
    $filesExist = false;

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['generate_files'])) {
            foreach ($types as $type => $label) {
                cmp_generate_files($type);
                $filesExist = true;
            }
        } elseif (isset($_POST['simulate_delete_images'])) {
            cmp_delete_media(true, 'images');
        } elseif (isset($_POST['confirm_delete_images'])) {
            cmp_delete_media(false, 'images');
        }
    }

    foreach ($types as $type => $label) {
        $url = get_option("cmp_non_offloaded_media_file_url_$type");
        if ($url && file_exists(WP_CONTENT_DIR . str_replace(content_url(), '', $url))) {
            $filesExist = true;
            break;
        }
    }

    return $filesExist;
}

function display_download_options()
{
    // Types de fichiers à gérer
    $types = ['all' => 'Tout', 'images' => 'Images', 'others' => 'Autres'];

    echo '<div class="cmp-card"><h3>Télécharger la liste selon le type de média :</h3><div class="cmp-grid">';

    foreach ($types as $type => $label) {
        $url = get_option("cmp_non_offloaded_media_file_url_$type");

        $path = WP_CONTENT_DIR . str_replace(content_url(), '', $url);

        echo "<div class='cmp-grid-item'><p class='cmp-label'>$label :</p>";

        // Si le fichier existe, affiche le bouton de téléchargement
        if (file_exists($path)) {
            echo "<a href='$url' class='button button-secondary' download>Télécharger $label</a>";
        } else {
            echo "<button class='button button-secondary' disabled>Télécharger $label</button>";
        }

        echo "</div>";
    }

    echo '</div></div>';
}

function display_deletion_options()
{
    $deleted_items = get_option('cmp_image_ids_to_delete', []);
    $disableButtons = empty($deleted_items) ? 'disabled' : '';

    echo "<div class='cmp-card'><h3>Suppression de ces éléments :</h3><div class='cmp-grid'>";
    echo "<div class='cmp-grid-item'>";
    echo "<p class='cmp-label'>Simuler la suppression des images :</p>";
    echo "<button id='simulate-delete' name='simulate_delete_images' class='button button-secondary' $disableButtons>Simuler la suppression</button>";
    echo "</div>";
    echo "<div class='cmp-grid-item'>";
    echo "<button id='real-delete' name='confirm_delete_images' class='button button-primary' $disableButtons>Confirmer la suppression</button>";
    echo "</div>";
    echo '</div></div>';
    echo '<div id="cmp-simulation-results"></div>';
}

function cmp_generate_files($type)
{
    global $wpdb;

    // Get les informations de la BDD de WP OFFLOAD MEDIA
    $prefix = $wpdb->prefix;
    $offloaded_media_ids = $wpdb->get_col("SELECT source_id FROM {$prefix}as3cf_items");
    $media_ids_list = !empty($offloaded_media_ids) ? implode(',', array_map('intval', $offloaded_media_ids)) : '0';

    // Tableau des formats d'images acceptés
    $image_extensions = ['jpg', 'jpeg', 'png', 'svg', 'webp', 'bmp', 'gif'];

    if ($type == 'images') {
        $like_clauses = array_map(
            function ($ext) {
                return "pm.meta_value LIKE '%.$ext'";
            },
            $image_extensions
        );
        $where_type = "AND (" . implode(' OR ', $like_clauses) . ")";
    } elseif ($type == 'others') {
        $not_like_clauses = array_map(
            function ($ext) {
                return "pm.meta_value NOT LIKE '%.$ext'";
            },
            $image_extensions
        );
        $where_type = "AND (" . implode(' AND ', $not_like_clauses) . ")";
    } else {
        $where_type = "";
    }

    // Requête finale
    $query = "SELECT p.ID, pm.meta_value AS file_path 
    FROM {$prefix}posts p 
    JOIN {$prefix}postmeta pm ON p.ID = pm.post_id 
    WHERE pm.meta_key = '_wp_attached_file' 
    AND p.post_type = 'attachment' 
    AND p.ID NOT IN ($media_ids_list) 
    $where_type";

    $non_offloaded_media = $wpdb->get_results($query);

    // Chemin et noms des fichiers CSV
    $filename = "non-offloaded-media-$type.csv";
    $file_path = wp_upload_dir()['basedir'] . '/' . $filename;
    $file_url = wp_upload_dir()['baseurl'] . '/' . $filename;

    // Génération du CSV
    $output = fopen($file_path, 'w');
    fputcsv($output, ['ID', 'Format', 'URL',]);
    $image_ids = [];
    foreach ($non_offloaded_media as $media) {
        if ($type === 'images') {
            $image_ids[] = $media->ID;
        }
        $media_url = wp_upload_dir()['baseurl'] . '/' . $media->file_path;
        $extension = strtolower(pathinfo($media->file_path, PATHINFO_EXTENSION));
        fputcsv($output, [$media->ID, $extension, $media_url]);
    }
    fclose($output);

    // Stocker les IDs des images pour la partie suppression
    if ($type === 'images') {
        update_option('cmp_image_ids_to_delete', $image_ids);
    }

    update_option("cmp_non_offloaded_media_file_url_$type", $file_url);
}

function cmp_handle_delete_media()
{
    // Vérifiez le nonce pour la sécurité
    check_ajax_referer('cmp_nonce', 'nonce');

    $simulate = isset($_POST['simulate']) && filter_var($_POST['simulate'], FILTER_VALIDATE_BOOLEAN);
    $type = isset($_POST['type']) ? sanitize_text_field($_POST['type']) : 'images'; // Type est toujours 'images' par défaut

    if ($type !== 'images') {
        wp_send_json_error(['message' => "Cette fonction ne gère que la suppression des images pour le moment."]);
        return;
    }

    $deleted_items = get_option('cmp_image_ids_to_delete', []);
    $total_ids = count($deleted_items);

    if ($simulate) {
        if ($total_ids > 0) {
            wp_send_json_success(['ids' => $deleted_items, 'total' => $total_ids, 'message' => "Simulation réussie. Nombre d'images à supprimer: $total_ids."]);
        } else {
            wp_send_json_success(['message' => "Aucun média image à supprimer."]);
        }
    } else {
        // ! Suppression des médias !
        foreach ($deleted_items as $attachment_id) {
            wp_delete_attachment($attachment_id, true);
        }
        delete_option('cmp_image_ids_to_delete'); // Évite une suppression répétée
        wp_send_json_success(['message' => "Médias images supprimés avec succès.", 'total' => $total_ids]);
    }
}
add_action('wp_ajax_simulate_delete_images', 'cmp_handle_delete_media');
add_action('wp_ajax_delete_media', 'cmp_handle_delete_media');
