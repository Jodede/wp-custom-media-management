<?php
/*
Plugin Name: Gestion Médias non offloaded
Description: Permet la génération d'un fichier listant les médias non externalisés (offload) et leur suppression.
Version: 2.0
Author: Jode
*/

require_once plugin_dir_path(__FILE__) . 'options-page.php';

function cmp_enqueue_scripts()
{
    wp_enqueue_style('cmp-style', plugins_url('/assets/css/cmp-style.css', __FILE__));
    wp_enqueue_script('cmp-ajax-script', plugins_url('/assets/js/cmp-ajax.js', __FILE__));
    wp_localize_script('cmp-ajax-script', 'cmpAjax', array(
        'ajaxUrl' => admin_url('admin-ajax.php'),
        'nonce' => wp_create_nonce('cmp_nonce'),
    ));
}
add_action('admin_enqueue_scripts', 'cmp_enqueue_scripts');
