# Plugin de Gestion Médias pour WordPress avec WP Offload Media

Ce plugin offre une solution personnalisée pour gérer les médias dans WordPress, spécialement conçu pour fonctionner avec le plugin WP Offload Media de Delicious Brains (Externalisation des médias et assets dans des buckets S3 ou autres).

## Fonctionnalités Principales

- Génère des fichiers CSV listant les médias non externalisés.
- Permet la simulation et la suppression des médias images non externalisés en se basant sur les métadonnées de WP Offload Media.

## Prérequis

Ce plugin nécessite :

- WordPress 5.0 ou supérieur.
- Le plugin WP Offload Media de Delicious Brains.

## Installation

1. Téléchargez et installez le plugin WP Offload Media.
2. Configurez WP Offload Media selon vos besoins.
3. Installez ce plugin de gestion médias via le téléversement de plugin ou en téléchargeant le dossier dans votre répertoire `wp-content/plugins`.
4. Activez le plugin via le menu "Plugins" dans WordPress.

## Configuration Importante

Si le plugin WP OFFLOAD MEDIA n'a pas généré ses METADATA en Offloadant les éléments sur le buckets, le plugin ne fonctionnera pas correctement.
Pour résoudre ce problème, vous devez activer une fonctionnalité spécifique dans WP Offload Media qui permet de générer et d'utiliser des métadonnées additionnelles.

### Activer l'outil "Add Metadata"

Vous trouverez la documentation complète de l'outil du plugin ici :
https://deliciousbrains.com/wp-offload-media/doc/add-metadata-tool/

Ajoutez la ligne suivante dans votre fichier `wp-config.php` :

```php
define( 'AS3CF_SHOW_ADD_METADATA_TOOL', true );
```

Ensuite, suivez ces étapes :

1. Allez dans l'onglet "Tools" du plugin WP Offload Media.
2. Cliquez sur "Add Metadata" pour commencer à générer les métadonnées nécessaires.
3. Une fois le processus terminé, cliquez sur "Find items with files missing in bucket and remove metadata, mark others verified" pour finaliser la configuration.

Après avoir suivi ces étapes, le plugin de gestion médias sera prêt à être utilisé avec toutes ses fonctionnalités.

## Utilisation

Une fois le plugin et WP Offload Media correctement configurés, vous pouvez :

- Accéder à la page de gestion des médias personnalisée via le tableau de bord WordPress.
- Utiliser les boutons fournis pour générer des fichiers CSV, simuler la suppression des médias images ou effectuer une suppression de ceux-ci.

## Support

Pour toute question ou problème lié à ce plugin, veuillez ouvrir un problème (issue) sur le dépôt GitLab/GitHub du projet.

## Licence

Ce plugin est distribué sous la licence GPL v2 ou ultérieure.
Tous les autres droits non expressément accordés dans la licence sont réservés.

## Authors

- **jdesgranges** - ** - [jdesgranges]() - **
- ChatGPT d'OpenAI - Co-auteur, a contribué à la documentation et au débogage.
